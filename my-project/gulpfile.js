var gulp = require('gulp'), //Подключаем Gulp
    sass = require('gulp-sass'),//Подключаем Sass пакет
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglifyjs'),
    cssnano = require('gulp-cssnano'), // Подключаем пакет для минификации CSS
    rename  = require('gulp-rename'), // Подключаем библиотеку для переименования файлов;
    del = require('del'),
    imagemin  = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
    pngquant  = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png
    cache  = require('gulp-cache'), // Подключаем библиотеку кеширования
    autoprefixer = require('gulp-autoprefixer');// Подключаем библиотеку для автоматического добавления префиксов

gulp.task('sass', function(){
    return gulp.src('app/sass/**/*.+(scss|sass)')
        .pipe(sass())
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7']))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.reload({stream:true}))
});

gulp.task('scripts', function(){
    return gulp.src([
        'app/libs/jquery/dist/jquery.min.js',
        'app/libs/bootstrap/dist/js/bootstrap.min.js',
        'app/libs/slick-carousel/slick/slick.min.js',
        'app/libs/jquery.selectBox/jquery.selectBox.js'
    ])
        .pipe(concat('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('app/js'));
});

gulp.task('cssLibs', ['sass'], function(){
    return gulp.src('app/css/libs.css')
        .pipe(cssnano())
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest('app/css'))
});

gulp.task('browser-sync', function(){
    browserSync({
        server: {
          baseDir:'app'
        },
        notify: false
    })
});

gulp.task('clean', function(){
    return del.sync('dist');
});

// gulp.task('clearCache', function(){
//     return cache.clearAll();
// });

gulp.task('img', function(){
    return gulp.src('app/img/**/*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('dist/img'));
});

gulp.task('watch', ['browser-sync', 'cssLibs', 'scripts'], function(){
    gulp.watch('app/sass/**/*.sass', ['sass']); // Наблюдение за sass файлами в папке sass
    gulp.watch('app/*.html', browserSync.reload); // Наблюдение за HTML файлами в корне проекта
    gulp.watch('app/js/**/*.js', browserSync.reload); // Наблюдение за JS файлами в папке js
});

gulp.task('build', ['clean', 'img', 'sass', 'scripts'], function() {

    var buildCss = gulp.src([ // Перенос библиотек в продакшн
        'app/css/style.css',
        'app/css/media.css',
        'app/css/libs.min.css'
    ])
        .pipe(gulp.dest('dist/css'))

    var buildFonts = gulp.src('app/fonts/**/*') // Переносим шрифты в продакшн
        .pipe(gulp.dest('dist/fonts'))

    var buildJs = gulp.src('app/js/**/*') // Перенос скриптов в продакшн
        .pipe(gulp.dest('dist/js'))

    var buildHtml = gulp.src('app/*.html') // Перенос HTML в продакшн
        .pipe(gulp.dest('dist'));

});


gulp.task('clear', function (callback) {
    return cache.clearAll(); //очистка кэша
})

gulp.task('default', ['watch']);

